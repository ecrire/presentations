﻿Stratégies de sélection de modules
============================================
:Auteur: Robin Millette
:Date: 15 octobre 2010
:Lieu: Montréal, Québec
:Événement: DrupalCamp Montréal 2010
:Licence: Creative Commons BY-SA 3.0
:URL: http://rym.waglo.com/presentations/dcm2010/

**VERSION DE TRAVAIL**

**WORK IN PROGRESS**

----

Robin Millette
===============
* Petit côté obsessif qui me pousse à découvrir *tous* les modules
* **Drupal** depuis 2005, **PHP** depuis 1999, **GNU/Linux** depuis 1995 (Slackware sur 647 disquettes...)
* Pitonne depuis 1985 (Timex Sinclair avec 2KiB de RAM)
* Ancien poète

----

Robin Millette et logiciel libre
==================================
* Logiciels libres et standards ouverts
* StatusNet (décentralisé)
* Observatoire de l'informatique libre québécois depuis 2008
* Président de FACIL en 2005-6
* SQIL de 2004 à 2008

----

Objectifs de cette présentation
=========================================
1) Voir des exemples et catégories de modules;
2) Explorer des critères et outils de sélection;
3) Évaluer les conséquences;
4) Contempler les alternatives;
5) Élaborer une stratégie efficace de sélection de modules.


----

On n'adressera pas...
========================
* Les modules un par un...
* Drupal 5

----


.notes: Je crois fermement aux bénéfices irremplaçables qu'apportent les logiciels libres et les standards ouverts


