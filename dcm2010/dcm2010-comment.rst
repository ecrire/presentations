﻿Critères de sélection (1/3)
===================================
* Fraîcheur
* Qualité de la documentation
* Facilité de prise en main
* Historique des développeur(s)
* Qualité du code source

----

Critères de sélection (2/3)
===================================
* Nombre de bugs ouverts
* Nombre d'installations (popularité)
* Fonctionnalités recherchées
* Performance / mise à l'échelle *scalability*

----

Critères de sélection (3/3)
===================================
* Historique de sécurité
* État des traductions (quand ça s'applique)
* Dépendances
* Flexibilité ou spécialité

----

Outils de sélection
===================================
Sites d'intérêt
---------------
* http://drupalmodules.com
* http://drupal.org
* http://groups.drupal.org/similar-module-review

----

http://drupal.org
==================
* Taxonomie
* Bloc **Related projects**
* Status

  * Recommended
  * Development Snapshot
  * Other releases (alpha and beta versions, for example)

----

http://drupal.org exemples
===========================
.notes: FIXME!!

All **6.x** modules in the **Organic Groups** *module category* ordered by number of installations:
  http://drupal.org/project/modules?page=1&filters=tid:90%20drupal_core:87&solrsort=sis_project_release_usage%20desc

Bleeding edge (newest modules):
  http://drupal.org/project/modules?filters=drupal_core:87&solrsort=created%20desc

----

http://drupal.org exemples (suite)
===================================
.notes: FIXME!!

.. image:: http://chart.apis.google.com/chart?chs=100x100&chld=H|1&cht=qr&chl=http%3A%2F%2Fxav.cc%2Fdcm2010-release
   :align: left
   :width: 150 px

Latest releases and by latest activity (currently being developped):

  * *~->* `by-release <http://drupal.org/project/modules?filters=drupal_core:87&solrsort=ds_project_latest_release_87%20desc>`_
  * *~->* `by-activity <http://drupal.org/project/modules?filters=drupal_core:87&solrsort=ds_project_latest_activity_87%20desc>`_

.. image:: http://chart.apis.google.com/chart?chs=100x100&chld=H|1&cht=qr&chl=http%3A%2F%2Fxav.cc%2Fdcm2010-activity
   :align: left
   :width: 150 px

----

Coder
===================================
.notes: FIXME!!

* from scratch
* php dans drupal
* patcher modules
* patcher core
* font va grossir quand on va retirer la ligne *.notes: ...* ci haut.

