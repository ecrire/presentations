Colophon
======================
:Titre: Stratégies de sélection de modules
:Auteur: Robin Millette
:Date: 15 octobre 2010
:Lieu: Montréal, Québec
:Événement: DrupalCamp Montréal 2010
:Licence: Creative Commons BY-SA 3.0
:URL: http://rym.waglo.com/presentations/dcm2010/
:Source: http://gitorious.org/ecrire/presentations/trees/master/dcm2010

À propos de cette présentation:
=================================
:Éditeur: vim
:Entrée: restructuredText (python docutils)
:Sortie: HTML 5 / Javascript / CSS 3
:Convertisseur: Landslide
:Highlight: Pygments

----

À propos de cette présentation
=================================
.notes: FIXME!!

vim
  http://vim.example.com

restructuredText
  http://docutils.example.com

HTML 5
  http://html5.example.com

Landslide
  http://landslide.example.com

Pygments
  http://pygments.example.com
