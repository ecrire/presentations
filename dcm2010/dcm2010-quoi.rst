﻿Drupal Core
============
* Un tarball, et après?
* Vocabulaires
* Utilisateurs
* Types de contenu
* Système de permissions

----

Faiblesses de Drupal Core
===========================
* Traitement des images et autres médias: absent;
* Utiliser et définir ses propres champs de données pour les types de contenu: absent;
* Blog, Forum, Agrégateur... pourraient être plus flexibles.

----

Statistiques
===================
Développement centralisé sur drupal.org en quelques chiffres:

* 6828 modules au total http://drupal.org/project/modules/index
* 594 modules pour Drupal 7.x http://drupal.org/project/modules/index?drupal_core=103
* 4843 modules pour Drupal 6.x http://drupal.org/project/modules/index?drupal_core=87
* 2654 modules pour Drupal 5.x http://drupal.org/project/modules/index?drupal_core=78
* 787 modules pour Drupal 4.7.x http://drupal.org/project/modules/index?drupal_core=79
* 3500 développeurs inscrits sur drupal.org http://drupal.org/profile/profile_drupal_module_developer

----

Les modules à la rescousse
==============================
Équivalents des plugins ou greffons, il en existe près de 7000 sur drupal.org.

----

Profiles d'installation
========================
.notes: FIXME!!

Assemblage de modules de façon à accomplir une tâche assez précise

Voir **Drush make** (autre présentation, par ABC)

----

Distributions
========================
.notes: FIXME!!

Dans certains cas, on va trouver des distributions basées sur Drupal ailleurs que sur drupal.org.

* openatrium
* news management (??)
* ...


