# Feeding Drupal

----

# In this presentation...

- Forget about the core aggregator
- Mappers, parsers, fetchers and processors
- Third party modules
- Why would you use it
- Questions?
- ...
- Layout is
- Good for about
- 9 bullet points

## Presenter Notes
Don't tell anyone!

Press "p" to open another window with the slides and presenter notes

----

# Is the core aggregator good enough?

- Don't need nodes
- Don't need taxonomy
- Short snippets from the content will be enough
- Only feeding on proper RSS and Atom
- Use the default Blog module and you use the little know quick aggregator to blog post feature
- Otherwise? Time to discover the Feeds universe

.notes: keep for myself and/or later, toggle on the slide by pressing "2"

----

# Feeds overview

- Parses many different feed types, not just RSS, Atom or XML
- Can store content as nodes or as lightweight Data (with the aptly-named Data module)
- Map any information to CCK feeds
- Import Users, Ubercart products, files, etc.
- Works through Cron or its own Queue (with optional module)
- Etc.

----

# Feeds architecture

- Fetchers (how are you getting the content)
- Parsers (what format is it)
- Processors (what are we creating/outputting)
- Mappers (map source fields to Drupal entities)


----

# Fetchers
#How are you getting the content


----

# Parsers
#What format is it


----

# Processors
#What are we creating/outputting

Lorem ipsum cataboumboum chicachica snap.

----

# Mappers
#Map source fields to Drupal entities

Slides with only a title, no content, take the middle of the screen.

Slides don't need a title at all. Next 2 slides illustrate this.

----


# Entering content

----

<img src='839Cx900y900.jpg' alt='Simpsons scene: thousand monkeys typing' />

----

# More

Paragraph...

----

# Fully supports Features import and export

Feeds works very well with the Features module. It even comes with a couple of examples.

----

# Programming with Feeds

* Object oriented
* ...

----

# References / URLs (1/?)

* **Feeds module:** <http://drupal.org/project/feeds>
* **Contributed plugin modules for Feeds:** <http://drupal.org/node/856644>
* **Feeds screenshots:** [http://www.flickr.com/photos/developmentseed/ sets/72157622725680906/](http://www.flickr.com/photos/developmentseed/sets/72157622725680906/)
* **Feeds screencasts:** [http://vimeo.com/developmentseed/videos/ search:aggregator%20feeds/sort:newest](http://vimeo.com/developmentseed/videos/search:aggregator%20feeds/sort:newest)

----

# References / URLs (2/?)

* **XML Path Language (XPath):** <http://www.w3.org/TR/xpath/>
* **PubSubHubbub (PuSH or "Don't pull so much from me!"):** <http://code.google.com/p/pubsubhubbub/>

----

# References / URLs (3/?)

* **Obsevatoire du libre (old school Feeds API):** <http://oilq.org/>
* **Muniduweb:** <http://muni.site.koumbit.net/>
* **Managing News:** <http://managingnews.com/>

----

# References / URLs (4/?)

* **Firefox Firebug:** http://...
* **Chrome Inspector:** http://...

----


# References / URLs (5/?)

* **XPath tutorial:** <http://www.tizag.com/xmlTutorial/xpathtutorial.php>
* **QueryPath tutorial:** [https://fedorahosted.org/querypath/wiki/ QueryPathTutorial](https://fedorahosted.org/querypath/wiki/QueryPathTutorial)

----



# Notes

* oilq -> many feeds (demo)
* managing news -> back-end (demo)
* muniduweb -> front-end (demo)
* atom field -> atom views
* features friendly
* Contributed plugin modules for Feeds http://drupal.org/node/856644
* xpath / tamper

----

.qr: 450|http://rym.waglo.com/presentations/feeding-drupal/

